require "csv"

input = File.read("input.csv")

direction = "N"
x = 0
y = 0

CSV.each_row(input) do |row|
    row.each do |column|
        column = column.strip

        direction = update_direction(direction, column[0,1])
        steps = column.sub(column[0,1], "").to_i

        case direction
        when "N"
            y += steps
        when "S"
            y -= steps
        when "E"
            x += steps
        when "W"
            x -= steps
        end
    end

    puts "Distance: " + (x.abs + y.abs).to_s
end

def update_direction(current_direction, turn)
    case {current_direction, turn}
    when {"N", "L"}
        "W"
    when {"N", "R"}
        "E"
    when {"E", "L"}
        "N"
    when {"E", "R"}
        "S"
    when {"S", "L"}
        "E"
    when {"S", "R"}
        "W"
    when {"W", "L"}
        "S"
    when {"W", "R"}
        "N"
    end
end
